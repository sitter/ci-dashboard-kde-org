/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 * SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
const txtFilter = document.getElementById('txtFilter');
const catFilter = document.getElementById('catFilter');

const dataArrSplit = /[",]+/;

const checkOne = (regex, category, app) => {
  const checkOneText = () => {
    if (!regex) {
      return true;
    }
      console.log(app.querySelector('.description').innerText.replace(/\s/g, ''))
    return regex.test(app.querySelector('.description').innerText.replace(/\s/g, ''));
  };

  const checkOneCategory = () => {
    if (category == 'all') {
      return true;
    }
    let categories = app.dataset['categories'].split(dataArrSplit);
    for (let cat of category.split(dataArrSplit)) {
      if (categories.indexOf(cat) > -1) {
          console.log(categories, cat)
          return true;
      }
    }
    return false;
  };

  return checkOneText() && checkOneCategory();
}

const filter = (text, category) => {
  const apps = document.querySelectorAll('.product');
  const regex = text ? new RegExp(text.replace(/\s/g, ''), 'i') : null;
  apps.forEach(function(app) {
    if (checkOne(regex, category, app)) {
      // app is kept
      app.classList.remove('d-none');
    } else {
      app.classList.add('d-none');
    }
  });
}

txtFilter.addEventListener('input', (event) => filter(event.target.value, catFilter.value));
catFilter.addEventListener('change', (event) => filter(txtFilter.value, event.target.value));

filter(txtFilter.value, catFilter.value);
