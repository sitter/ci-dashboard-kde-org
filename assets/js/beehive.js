// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

(function () {
  const beehiveAlign = function () {
    let rows = []
    let row = 0
    let lastLeft = Number.MIN_SAFE_INTEGER
    const cells = document.querySelectorAll('.honeycomb')
    cells.forEach((honeycomb) => {
      const position = honeycomb.getBoundingClientRect();
      if (position.left < lastLeft) {
        row++
      }
      lastLeft = position.left

      honeycomb.classList.remove('indented-row')
      honeycomb.classList.remove('scoot-up-row')

      rows[row] ||= []
      rows[row].push(honeycomb)
      // TODO need to replace the previous classes somehow but replace() mucks it all up
    })

    function isOdd(n) {
      return Math.abs(n % 2) == 1;
    }

    rows.forEach((row, i) => {
      row.forEach((honeycomb) => {
        if (isOdd(i)) {
          honeycomb.classList.add('indented-row')
        }
        // TODO the last item in an odd row is shifted off to the side, it should really be in the next row
        if (i != 0) {
          honeycomb.classList.add('scoot-up-row')
        }
      })
    })
  };

  beehiveAlign();
  window.addEventListener('resize', beehiveAlign, false);
})();
