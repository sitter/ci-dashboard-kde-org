#!/bin/sh

# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

set -eu

git clean -fdx
git pull --rebase

ruby pacman/main.rb
hugo --minify

cp -R public/* /var/www/html/kdeqt6/dashboard/
