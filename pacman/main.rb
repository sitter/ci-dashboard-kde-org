# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

require 'faraday'
require 'yaml'
require "graphql/client"
require "graphql/client/http"

module GAPI
  HTTP = GraphQL::Client::HTTP.new("https://invent.kde.org/api/graphql") do
    def headers(context)
      { "User-Agent": "Volker" }
    end
  end

    # GraphQL::Client.dump_schema(HTTP, "#{__dir__}/schema.json")
  Schema = GraphQL::Client.load_schema("#{__dir__}/schema.json")
  Client = GraphQL::Client.new(schema: Schema, execute: HTTP)
end
Query = GAPI::Client.parse(File.read("#{__dir__}/anotherquery.graphql"))

connection = Faraday.new(url: 'https://projects.kde.org/') do |builder|
  builder.response :json
  builder.response :raise_error
  builder.response :logger
  builder.headers['User-Agent'] = 'Volker'
end

response = connection.get('api/v1/projects/')
groups = response.body.group_by do |x|
  File.dirname(x)
end.keys
groups = groups.reject { |x| %w[historical neon neon/neon unmaintained sysadmin wikitolearn].any?(x) }

# TODO: more branches
doodads = groups.map { |group| [group, 'master'] }
all = []
all_per_branch = {'master' => []}
doodads.each do |groupName, ref|
  response = GAPI::Client.query(Query, variables: {
    groupName: groupName,
    ref: ref
  })

  data = Marshal.load(Marshal.dump(response.to_h['data']['group']['projects']))
  data.fetch('nodes').each do |project|
    project['basename'] = File.basename(project.fetch('fullPath'))

    # NOTE: fetch(0) may be nil when there is no pipeline of that type
    success = project.fetch('lastSuccess').fetch('nodes').fetch(0, nil)
    success_date = Time.parse(success.fetch('finishedAt')) if success

    failure = project.fetch('lastFailure').fetch('nodes').fetch(0, nil)
    failure_date = Time.parse(failure.fetch('finishedAt')) if failure

    project['latestPipeline'] = if success && !failure
                                  success
                                elsif !success && failure
                                  failure
                                elsif !success && !failure
                                  {}
                                else
                                  success_date > failure_date ? success : failure
                                end
  end
  data.merge!('groupName'=> groupName, 'ref'=> ref)

  all += data.fetch('nodes')
  all_per_branch[ref] += data.fetch('nodes')
  File.open("#{__dir__}/../content/#{groupName.tr('/', '-')}.#{ref}.md", 'w') do |file|
    file.write(YAML.dump(data))
    file.write("\n---\n")
  end
end

all_per_branch.each do |ref, nodes|
  File.open("#{__dir__}/../content/all.#{ref}.md", 'w') do |file|
    file.write(YAML.dump('groupName' => 'all', 'ref' => ref, 'nodes' => nodes))
    file.write("\n---\n")
  end
end

File.open("#{__dir__}/../content/all.md", 'w') do |file|
  file.write(YAML.dump('groupName' => 'all', 'ref' => 'all', 'nodes' => all))
  file.write("\n---\n")
end

File.open("#{__dir__}/../content/beehive.md", 'w') do |file|
  file.write(YAML.dump('layout' => 'beehive', 'groupName' => 'Beehive', 'ref' => 'all', 'nodes' => all))
  file.write("\n---\n")
end
