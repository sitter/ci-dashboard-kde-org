module develop-kde-org

go 1.14

require (
	invent.kde.org/websites/aether-sass v0.0.0-20220225202045-d8a759e30746 // indirect
	invent.kde.org/websites/hugo-kde v0.0.0-20230903004919-33a014c12f02 // indirect
)
